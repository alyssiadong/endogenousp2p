# include("src/EndogenousP2P.jl")
using EndogenousP2P

### Creating the power network model
pn = EndogenousP2P.powerNetwork(string(EndogenousP2P.PROJECT_ROOT,"/test/matpower/case39b_31a_bis.m"))
# result = solve_and_plot_OPF(pn)

N_prodcons = [count(pn.type_assets .== "prod"),count(pn.type_assets .== "cons")]

rho = 1.0
ΔT = 8000.0
sendTypePeers = "sendback"
sendTypeSO = "sendback"
iterMatchingPeers = "yesmatching"
iterMatchingSOPeers = "yesmatching"
iterMatchingSOLossP = "yesmatching"
priceUpdate = "new"
tradeUpdate = "new"
powerSOUpdate = "new"
dualvarSOUpdate = "new"

δt = 50
Tsimu = 1000000
OSQPeps=1e-8

α = 90.0
β = 10.0
σ = 0.0
gamma = 0
solver = "OSQP"
delta = 1.0
delta_SO = 1.0

Eprim = 1e-6

Ntrig = [if (x == "prod") Int(ceil((N_prodcons[2]+1 )*delta))
			elseif (x == "cons") Int(ceil((N_prodcons[1]+1 )*delta))
			else Int(ceil(sum(N_prodcons)*delta_SO)) end 
			for x in pn.type_assets]
Tmax = fill(ΔT, length(Ntrig))
compDelays = zeros(length(Ntrig))
compDelays[end] = 3.0			# SO

aPar = EndogenousP2P.algoParams(
		rho, gamma, OSQPeps, solver, Ntrig, Tmax, 
		sendTypePeers,
		sendTypeSO,
		iterMatchingPeers,
		priceUpdate,
		tradeUpdate,
		iterMatchingSOPeers,
		iterMatchingSOLossP,
		powerSOUpdate,
		dualvarSOUpdate )
nPar = EndogenousP2P.commParams(α, β, σ)
cPar = EndogenousP2P.compParams(compDelays)

t_elapsed = @elapsed begin
	mem = EndogenousP2P.run_simulation(Tsimu, Eprim, δt, pn, nPar, cPar, aPar)
end
